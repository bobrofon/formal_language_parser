Простой рекурсивный парсер контекстно-свободной грамматики. Производит нисходящий синтаксический анализ заданного выражения путём моделирования левых выводов.

==============================

Python3

$ python3 cfg_parse.py -h
usage: cfg_parse.py [-h] [-c file_name] [--right] expr

Parser for context-free languages.

positional arguments:
  expr          Expression for recognition

optional arguments:
  -h, --help    show this help message and exit
  -c file_name  Config with CFG (Default: config.json)
  --right       using right-hand PDA

==============================

Пример конфигурации

Грамматика задаётся через конфигурационный файл в json формате.

Грамматика:
G = (N, Z, P, S)
N = {E, T, F} - нетерминальные символы
Z = {a, +, *, (, )} - терминальные символ
S = E - стартовый символ
P: - множество правил
	1. E->E+T
	2. E->T
	3. T->T*F
	4. T->F
	5. F->(E)
	6. F->a

Конфигурационный файл для грамматики:
{
	"terminals": "a+*()",
	"nonterminals": "ETF",
	"start_term": "E",
	"rules": {
		"E": ["E+T", "T"],
		"T": ["T*F", "F"],
		"F": ["(E)", "a"]
	}
}

==============================

Пример использования

$ python3 cfg_parse.py 'a+a*a'
Execution info:
Namespace(config_file_name='config.json', expr='a+a*a', pda_type='left')
{'nonterminals': 'ETF', 'terminals': 'a+*()', 'start_term': 'E', 'rules': {'E': ['E+T', 'T'], 'T': ['T*F', 'F'], 'F': ['(E)', 'a']}}

Result:
( a+a*a, E )
	|-- ( a+a*a, E+T )
	|-- ( a+a*a, T+T )
	|-- ( a+a*a, F+T )
	|-- ( a+a*a, a+T )
	|-- ( +a*a, +T )
	|-- ( a*a, T )
	|-- ( a*a, T*F )
	|-- ( a*a, F*F )
	|-- ( a*a, a*F )
	|-- ( *a, *F )
	|-- ( a, F )
	|-- ( a, a )
	|-- ( ,  )

Trace:
E -0-> E+T -1-> T+T -2-> F+T -3-> a+T -4-> a+T*F -2-> a+F*F -3-> a+a*F -3-> a+a*a 
Rules:
	0: E->E+T
	1: E->T
	2: T->F
	3: F->a
	4: T->T*F

Примичание:
Result - последовательность тактов через которые проходит автомат. Описание такта: |--(входная_последовательность, состояние_стека).
Trace - последовательность переходов, выводящих заданное выражение в грамматике. После трейса привидены номера применённых правил.

Пример №2
$ python3 cfg_parse.py -c braces_config.json '(()(()))()'
Execution info:
Namespace(config_file_name='braces_config.json', expr='(()(()))()', pda_type='left')
{'rules': {'S': ['(S)S', 'S(S)', '']}, 'start_term': 'S', 'terminals': '()', 'nonterminals': 'S'}

Result:
( (()(()))(), S )
	|-- ( (()(()))(), (S)S )
	|-- ( ()(()))(), S)S )
	|-- ( ()(()))(), (S)S)S )
	|-- ( )(()))(), S)S)S )
	|-- ( )(()))(), )S)S )
	|-- ( (()))(), S)S )
	|-- ( (()))(), (S)S)S )
	|-- ( ()))(), S)S)S )
	|-- ( ()))(), (S)S)S)S )
	|-- ( )))(), S)S)S)S )
	|-- ( )))(), )S)S)S )
	|-- ( ))(), S)S)S )
	|-- ( ))(), )S)S )
	|-- ( )(), S)S )
	|-- ( )(), )S )
	|-- ( (), S )
	|-- ( (), (S)S )
	|-- ( ), S)S )
	|-- ( ), )S )
	|-- ( , S )
	|-- ( ,  )

Trace:
S -0-> (S)S -0-> ((S)S)S -1-> (()S)S -0-> (()(S)S)S -0-> (()((S)S)S)S -1-> (()(()S)S)S -1-> (()(())S)S -1-> (()(()))S -0-> (()(()))(S)S -1-> (()(()))()S -1-> (()(()))() 
Rules:
	0: S->(S)S
	1: S->

Пример №3 (правосторонний автомат)
$ python3 cfg_parse.py 'a+a*a' --right
Execution info:
Namespace(config_file_name='config.json', expr='a+a*a', pda_type='right')
{'terminals': 'a+*()', 'start_term': 'E', 'nonterminals': 'ETF', 'rules': {'F': ['(E)', 'a'], 'T': ['T*F', 'F'], 'E': ['E+T', 'T']}}

Result:
( a+a*a,  )
	|-- ( +a*a, a )
	|-- ( +a*a, F )
	|-- ( +a*a, T )
	|-- ( +a*a, E )
	|-- ( a*a, E+ )
	|-- ( *a, E+a )
	|-- ( *a, E+F )
	|-- ( *a, E+T )
	|-- ( a, E+T* )
	|-- ( , E+T*a )
	|-- ( , E+T*F )
	|-- ( , E+T )
	|-- ( , E )

Trace:
E -0-> E+T -1-> E+T*F -2-> E+T*a -3-> E+F*a -2-> E+a*a -4-> T+a*a -3-> F+a*a -2-> a+a*a
Rules:
	0: E->E+T
	1: T->T*F
	2: F->a
	3: T->F
	4: E->T