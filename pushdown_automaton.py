class pushdown_automation:
	def __init__(self, terminals, nonterminals, start_term, rules):
		self.terminals = terminals
		self.nonterminals = nonterminals
		self.start_term = start_term
		self.rules = rules
		self.stack = []
		self.expr = ''
		self.history = []

	def parse(self, expr):
		self.expr = expr
		self.stack = [self.start_term]
		return self.__exec()

	def __exec(self, last_rule=''):
		recursive_result = False

		if not self.stack and not self.expr: recursive_result = True
		elif not self.stack: recursive_result = False
		elif not self.__compare_stack_with_expr(): recursive_result = False
		elif self.stack[-1] in self.terminals: recursive_result = self.__exec_terminal()
		elif self.stack[-1] in self.nonterminals: recursive_result = self.__exec_nonterminal()

		if recursive_result:
			self.history.append({'expr': self.expr, 'stack': ''.join(reversed(self.stack)), 'rule': last_rule})

		return recursive_result

	def __exec_terminal(self):
		stack_ch = self.stack[-1]
		expr_ch = self.expr[0]

		if stack_ch != expr_ch:
			return False

		# update stack and expression
		self.stack.pop()
		self.expr = self.expr[1:]
		recursive_result = self.__exec()
		# restore stack and expression
		self.stack.append(stack_ch)
		self.expr = expr_ch + self.expr

		return recursive_result

	def __exec_nonterminal(self):
		nonterminal = self.stack[-1]
		self.stack.pop()
		recursive_result = False

		for rule in self.rules[nonterminal]:
			# update stack
			rule_chars = list(rule)
			rule_chars.reverse()
			self.stack += rule_chars
			recursive_result = self.__exec('{0}->{1}'.format(nonterminal, rule))
			#restore stack
			if len(rule_chars) > 0: self.stack = self.stack[:-len(rule_chars)]

			if recursive_result: break

		self.stack.append(nonterminal)
		return recursive_result

	# True if stack contains terminals only from expr
	# False unless
	def __compare_stack_with_expr(self):
		stack_terminals = [ch for ch in self.stack if ch in self.terminals]
		stack_terminals.reverse()
		expr_index = 0
		for ch in stack_terminals:
			expr_index = self.expr.find(ch, expr_index)
			if expr_index == -1: return False
			expr_index += 1
		return True