class right_pushdown_automation:
	def __init__(self, terminals, nonterminals, start_term, rules):
		self.terminals = terminals
		self.nonterminals = nonterminals
		self.start_term = start_term
		self.rules = self.__rules_reverse(rules)
		self.stack = ''
		self.expr = ''
		self.original_expr = ''
		self.history = []

	def parse(self, expr):
		self.expr = expr
		self.original_expr = expr
		self.stack = ''
		return self.__exec()

	def __exec(self, last_rule=''):
		recursive_result = False

		if self.stack is self.start_term and not self.expr: recursive_result = True
		elif not self.__compare_stack_with_expr(): recursive_result = False
		else:
			if not recursive_result: recursive_result = self.__exec_nonterminal()
			if not recursive_result and self.expr: recursive_result = self.__exec_terminal()

		if recursive_result:
			self.history.append({'expr': self.expr, 'stack': self.stack, 'rule': last_rule})

		return recursive_result

	def __exec_terminal(self):
		expr_ch = self.expr[0]

		# update stack and expression
		self.expr = self.expr[1:]
		self.stack = self.stack + expr_ch
		recursive_result = self.__exec()
		# restore stack and expression
		self.stack = self.stack[:-1]
		self.expr = expr_ch + self.expr

		return recursive_result

	def __exec_nonterminal(self):
		recursive_result = False

		for rule in self.rules.keys():
			if self.stack.endswith(rule):
				if len(rule) > 0: self.stack = self.stack[:-len(rule)]

				for nonterminal in self.rules[rule]:
					self.stack = self.stack + nonterminal
					recursive_result = self.__exec('{0}->{1}'.format(nonterminal, rule))
					self.stack = self.stack[:-1]
					if recursive_result: break

				self.stack = self.stack + rule
			if recursive_result: break

		return recursive_result

	# True if stack can produce expression
	# False unless
	def __compare_stack_with_expr(self):
		return len(self.original_expr) >= len(self.stack)

	@staticmethod
	def __rules_reverse(rules):
		reversed_rules = {}
		for nonterminal, expressions in rules.items():
			for expression in expressions:
				if expression not in reversed_rules:
					reversed_rules[expression] = set()
				reversed_rules[expression].add(nonterminal)
		return reversed_rules
