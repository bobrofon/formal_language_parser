import argparse
import json
import pushdown_automaton
import right_pushdown_automation


def main():
	# cl_parser - command line parser
	cl_parser = argparse.ArgumentParser(description='Parser for context-free languages.')
	cl_parser.add_argument('expr', help='Expression for recognition')
	cl_parser.add_argument('-c', metavar='file_name', dest='config_file_name',
	                       default='config.json', help='Config with CFG (Default: config.json)')
	cl_parser.add_argument('--right', dest='pda_type', action='store_const',
                   const='right', default='left', help='using right-hand PDA')
	args = cl_parser.parse_args()

	with open(args.config_file_name) as config_file:
		config = json.load(config_file)

	print('Execution info:')
	print(args)
	print(config)
	print()

	pda_types = {
		'left': pushdown_automaton.pushdown_automation,
	    'right': right_pushdown_automation.right_pushdown_automation
	}
	trace_types = {
		'left': print_trace,
	    'right': print_right_trace
	}

	cfg_parser = pda_types[args.pda_type](config['terminals'], config['nonterminals'], config['start_term'], config['rules'])
	is_found = cfg_parser.parse(args.expr)

	if is_found:
		print('Result:')
		print_history(cfg_parser.history)
		print('\nTrace:')
		trace_types[args.pda_type](cfg_parser.history)
	else:
		print('Expression "{0}" was not recognized'.format(args.expr))

def print_history(history):
	print('( {0}, {1} )'.format(history[-1]['expr'], history[-1]['stack']))
	for step in history[-2::-1]:
		print('\t|-- ( {0}, {1} )'.format(step['expr'], step['stack']))

def print_trace(history):
	stored_chars = []
	stored_rules = []
	print(history[-1]['stack'], end=' ')
	for pos in range(len(history)-2, -1, -1):
		step = history[pos]
		rule = step['rule']

		if not rule:
			stored_chars.append(history[pos+1]['stack'][0])
		else:
			if rule not in stored_rules: stored_rules.append(rule)
			print('-{0}-> {1}{2}'.format(stored_rules.index(rule), ''.join(stored_chars), step['stack']), end=' ')

	print('\nRules:')
	for pos in range(len(stored_rules)):
		print('\t{0}: {1}'.format(pos, stored_rules[pos]))

def print_right_trace(history):
	stored_chars = ""
	stored_rules = []
	last_rule = history[0]['rule']

	print(history[0]['stack'], end=' ')
	for i in range(1, len(history)):
		step = history[i]

		if not step['rule'] and step['stack']:
			stored_chars = step['stack'][-1] + stored_chars
		else:
			if last_rule not in stored_rules: stored_rules.append(last_rule)
			print('-{0}-> {1}{2}'.format(stored_rules.index(last_rule), step['stack'], stored_chars), end=' ')
			last_rule = step['rule']

	print('\nRules:')
	for pos in range(len(stored_rules)):
		print('\t{0}: {1}'.format(pos, stored_rules[pos]))

if __name__ == '__main__':
	main()